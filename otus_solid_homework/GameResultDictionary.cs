﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public class GameResultDictionary
    {
        private Dictionary<GameResult, string> resultDictionary = new Dictionary<GameResult, string>();
        public GameResultDictionary()
        {
            resultDictionary.Add(GameResult.Win, "You win the game!!");
            resultDictionary.Add(GameResult.Lose, "Game Over!!");
            resultDictionary.Add(GameResult.Less, "Guessed number is less!!");
            resultDictionary.Add(GameResult.Above, "Guessed number is above!!");
        }

        public string GetResult(GameResult gameResult)
        {
            return resultDictionary[gameResult];
        }
    }
}
