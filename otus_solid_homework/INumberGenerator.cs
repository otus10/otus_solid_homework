﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public interface INumberGenerator
    {
        public int GenerateNumber(int MinNumber, int MaxNumber);
    }
}
