﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public class OptionsConsole : IOptions
    {
        public int TryCount { get; set; }
        public int MinNumber { get; set; }
        public int MaxNumber { get; set; }

        public OptionsConsole SetMaxNumber(int maxNumber)
        {
            MaxNumber = maxNumber;
            return this;
        }

        public OptionsConsole SetMinNumber(int minNumber)
        {
            MinNumber = minNumber;
            return this;
        }

        public OptionsConsole SetTryCount(int tryCount)
        {
            TryCount = tryCount;
            return this;
        }

    }
}
