﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public class NumberGenerator : INumberGenerator
    {
        public virtual int GenerateNumber(int MinNumber, int MaxNumber)
        {
            Random random = new Random();
            return random.Next(MinNumber, MaxNumber);
        }
    }
}
