﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public class OptionsConsoleManager : IOptionsManager
    {
        private OptionsConsole _optionsConsole;
        public IOptions Options => _optionsConsole;
        public IOptionsManager CreateOptions()
        {
            _optionsConsole = new OptionsConsole();
            return this;
        }
        public IOptionsManager SetParameters()
        {
            SetTryCount();
            SetMinNumber();
            SetMaxNumber();
            return this;
        }
        private OptionsConsoleManager SetMaxNumber()
        {
            int maxNumber = Utility.ReadInt("Enter maximum number: ");
            _optionsConsole.SetMaxNumber(maxNumber);
            return this;
        }

        private OptionsConsoleManager SetMinNumber()
        {
            int minNumber = Utility.ReadInt("Enter minimum number: ");
            _optionsConsole.SetMinNumber(minNumber);
            return this;
        }

        private OptionsConsoleManager SetTryCount()
        {
            int tryCount = Utility.ReadInt("Enter try count: ");
            _optionsConsole.SetTryCount(tryCount);
            return this;
        }
    }
}
