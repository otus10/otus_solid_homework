﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public interface IOptionsManager
    {
        IOptions Options { get; }
        IOptionsManager CreateOptions();
        IOptionsManager SetParameters();

    }
}
