﻿using System;

namespace otus_solid_homework
{
    class Program
    {
        static void Main(string[] args)
        {
            IOptionsManager gameOptions = new OptionsConsoleManager();
            INumberGenerator numberGenerator = new NumberGenerator();
            //  INumberGenerator numberGenerator = NumberGeneratorMin.GetInstance();
            GameManager gameManager = new GameManager(gameOptions, numberGenerator)
                                    .Build()
                                    .Start();
            Console.ReadKey();

        }
    }
}
