﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public class Game
    {
        private readonly int _guessNumber;

        private int _tryCount;
        public int GuessNumber => _guessNumber;
        public Game(int guessNumber, int tryCount)
        {
            _guessNumber = guessNumber;
            _tryCount = tryCount;
        }

        public GameResult CheckGameResult(int guessNumber)
        {
            GameResult gameResult = CheckGuessNumber(guessNumber);
            if (gameResult == GameResult.Win)
                return GameResult.Win;

            DecrementTryCount();

            if (_tryCount == 0)
                return GameResult.Lose;
            else
                return gameResult;
        }
        private GameResult CheckGuessNumber(int guessNumber)
        {
            GameResult result = _guessNumber > guessNumber ? GameResult.Above :
                                   _guessNumber < guessNumber ? GameResult.Less
                                   : GameResult.Win;

            return result;
        }

        private void DecrementTryCount()
        {
            --_tryCount;
        }
    }
}
