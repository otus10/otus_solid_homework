﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public static class Utility
    {
        public static int ReadInt(string inputText)
        {
            Console.WriteLine(inputText);
            string input = Console.ReadLine();
            bool success = int.TryParse(input, out int result);
            if (success)
                return result;
            else
                throw new ArgumentException($"'{input}' cant be parsed to int.");
        }
    }
}
