﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    public class GameManager
    {
        private Game _game { get; set; }
        private IOptionsManager _gameOptionsManager { get; set; }
        private INumberGenerator _numberGenerator { get; set; }
        private readonly GameResultDictionary _gameResultDictionary;

        public GameManager(IOptionsManager gameOptionsManager, INumberGenerator numberGenerator)
        {
            this._gameOptionsManager = gameOptionsManager;
            this._numberGenerator = numberGenerator;
            _gameResultDictionary = new GameResultDictionary();
        }

        public GameManager Build()
        {
            Console.WriteLine("Please setup options.");
            _gameOptionsManager
                .CreateOptions()
                .SetParameters();
            return this;
        }

        public GameManager Start()
        {
            _game = new Game(_numberGenerator.GenerateNumber(_gameOptionsManager.Options.MinNumber,
                                                             _gameOptionsManager.Options.MaxNumber),
                            _gameOptionsManager.Options.TryCount
                                                            );
            Console.WriteLine("Game is started.");
            Turn();
            return this;
        }

        public void Turn()
        {
            int guessNumber = Utility.ReadInt("Guess a number: ");
            GameResult result = _game.CheckGameResult(guessNumber);
            Finish(result);
        }
        private void Finish(GameResult gameResult)
        {
            Console.WriteLine(_gameResultDictionary.GetResult(gameResult));
          
            if (gameResult.HasFlag (GameResult.Win) || gameResult.HasFlag(GameResult.Lose))
            {
                Console.WriteLine("Guessed number was {0}", _game.GuessNumber); 
            }
            else
            {
                Turn();
            };

        }
    }
}
