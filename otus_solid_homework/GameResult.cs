﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_solid_homework
{
    [Flags]
    public enum GameResult
    {
        Win = 1,
        Lose = 2,
        Less = 4,
        Above = 8,
    }
}
